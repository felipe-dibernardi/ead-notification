package com.ead.notification.controllers;

import com.ead.notification.dtos.NotificationDTO;
import com.ead.notification.models.NotificationModel;
import com.ead.notification.services.NotificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
public class UserNotificationController {

    private final NotificationService notificationService;

    @PreAuthorize("hasAnyRole('STUDENT')")
    @GetMapping("/user/{userId}/notifications")
    public ResponseEntity<Page<NotificationModel>> getAllNotificationsByUser(@PathVariable(value = "userId") UUID userId,
                                                                             @PageableDefault(page = 0, size = 10, sort = "notificationId",
                                                                                     direction = Sort.Direction.ASC) Pageable pageable) {
        return ResponseEntity.status(HttpStatus.OK).body(notificationService.findAllNotificationsByUser(userId, pageable));
    }

    @PreAuthorize("hasAnyRole('STUDENT')")
    @PutMapping("/user/{userId}/notifications/{notificationId}")
    public ResponseEntity<Object> updateNotification(@PathVariable(value = "userId") UUID userId,
                                                     @PathVariable(value = "notificationId") UUID notificationId,
                                                     @RequestBody @Valid NotificationDTO notificationDTO) {
        Optional<NotificationModel> notificationOpt =
                notificationService.findByNotificationIdAndUserId(notificationId, userId);
        if (notificationOpt.isPresent()) {
            notificationOpt.get().setNotificationStatus(notificationDTO.getNotificationStatus());
            notificationService.saveNotification(notificationOpt.get());
            return ResponseEntity.status(HttpStatus.OK).body(notificationOpt.get());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Notification not found!");
    }

}
